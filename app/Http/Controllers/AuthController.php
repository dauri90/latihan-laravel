<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('halaman.register');
    }
    public function kirim(Request $request){
        $nama = $request['fname'] . " " . $request['lname'];
        $gender = $request['gen'];
        $nation = $request['nation'];
        $language = $request['lang'];
        $bio = $request['bio'];
        return view('halaman.welcome2', compact ('nama','gender','nation','language','bio'));

    }
}
