<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Welcome</title>
</head>
<body>
    <h1>Selamat Datang!</h1>
    <p>Nama: {{$nama}} </p>
    <p>Jenis Kelamin: {{$gender}} </p>
    <p>Kebangsaan: {{$nation}} </p>
    <p>Bahasa: {{$language}}</p>
    <p>Biodata: {{$bio}} </p>

    <h4>Terima kasih telah bergabung di Website Kami. Media Belajar kita bersama!</h4>
</body>
</html>
