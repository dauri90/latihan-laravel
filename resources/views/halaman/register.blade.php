@extends('layout.master')

@section('judul')
Buat Account Baru
@endsection

@section('content')
<h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
    @csrf
    <label>First name:</label><br><br>
    <input type="text" name="fname"><br><br>
    <label>Last name:</label><br><br>
    <input type="text" name="lname"><br><br>
    <label>Gender:</label><br><br>
    <input type="radio" name="gen" value="male"> Male <br>
    <input type="radio" name="gen" value="female"> Female <br>
    <input type="radio" name="gen" value="other"> Other <br>
    <br>
    <label>Nationally</label><br><br>
    <select name="nation">
        <option value="Indonesian">Indonesian</option>
        <option value="Amerika">Amerika</option>
        <option value="Inggris">Inggris</option>
    </select><br><br>

    <label>Language Spoken:</label><br><br>
    <input type="checkbox" name="lang" value="Bahasa Indonesia"> Bahasa Indonesia <br>
    <input type="checkbox" name="lang" value="English"> English <br>
    <input type="checkbox" name="lang" value="Other"> Other <br>
    <br>
    <label>Bio:</label><br><br>
    <textarea name="bio" cols="30" rows="10"></textarea><br><br>

    <input type="submit" value="Kirim">

    </form>

@endsection
